from django.urls import path
from .import views

app_name = "polls"
urlpatterns = [

    path('',views.IndexView.as_view(), name='index'),
    path('slika/<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('slika/<int:image_id>/upvote', views.upvote, name="upvote"),
    path('slika/<int:image_id>/downvote', views.downvote, name="downvote")
]