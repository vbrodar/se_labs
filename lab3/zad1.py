grades = { "Pero": [2,3,3,4,3,5,3],
           "Djuro" : [4,4,4],
           "Marko" : [3,3,2,3,5,1]
           }
           
# Ispisi ime studenta koji ima najvisi prosjek. Zadatak treba proci kroz 
# sve studente, izracunati prosjek i ispisati ime studenta s navisim prosjekom.

# -- vas kod ide ispod -- #
import statistics

def average(items):
    total = float (sum(items))
    return total/len(items)

max= 0
maxValue=''
br=0
for key,values in grades.items():
    average=0
    for element in values:
        average += element
        br+=1
    average=average/br
    if max<average:
        max=average
        maxValue= key
    br=0
print(max,maxValue)

