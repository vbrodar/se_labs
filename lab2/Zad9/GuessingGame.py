import random

a = random.randint(1,9)

number = int(input(" Guess a number between 1 and 9: "))

while number != a:
    if number > a:
        print(" Overshot ")
        number = int(input(" Guess again : "))
    elif number < a:
            print(" Too low ")
            number = int(input(" Guess again : "))
else:
                print(" Well done you guessed number! ")