def find(orderedList,elementToFind):
    for element in orderedList:
        if element == elementToFind:
            return True
         
    return False



x = [1, 2, 4, 6, 9, 11, 13, 15, 17, 19]

print(find(x, 5))
print(find(x, 10))
print(find(x, 13))
print(find(x, 19))
print(find(x, -2))
